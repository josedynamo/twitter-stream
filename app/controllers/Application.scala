package controllers

import play.api.Play.current
import play.api._
import play.api.libs.json.JsValue
import play.api.libs.oauth.{ConsumerKey, RequestToken}
import play.api.mvc._
import actors.TwitterStreamer

class Application extends Controller {

  def credentials: Option[(ConsumerKey, RequestToken)] = for {
    apiKey <- Play.configuration.getString("twitter.apiKey")
    apiSecret <- Play.configuration.getString("twitter.apiSecret")
    token <- Play.configuration.getString("twitter.token")
    tokenSecret <- Play.configuration.getString("twitter.tokenSecret")
  } yield (
    ConsumerKey(apiKey, apiSecret),
    RequestToken(token, tokenSecret)
  )


  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }


  def tweets = WebSocket.acceptWithActor[String, JsValue] {
    request => out => TwitterStreamer.props(out)
  }

//  def tweets = Action.async {
//
//
//
//    credentials.map { case (consumerkey, requestToken) =>
//      val (iteratee, enumerator) = Concurrent.joined[Array[Byte]]
//      val jsonStream: Enumerator[JsObject] =
//        enumerator &>
//        Encoding.decode() &>
//        Enumeratee.grouped(JsonIteratees.jsSimpleObject)
//
//      val loginIteratee = Iteratee.foreach[JsObject] {
//        value => Logger.info(value.toString)
//      }
//
//      jsonStream run loginIteratee
//
//      WS
//        .url("https://stream.twitter.com/1.1/statuses/filter.json")
//        .sign(OAuthCalculator(consumerkey, requestToken))
//        .withQueryString("track" -> "reactive")
//        .get {
//          response =>
//            Logger.info("Status: " + response.status)
//            iteratee
//        }.map { _ =>
//        Ok("Stream closed")
//      }
//    } getOrElse {
//      Future.successful {
//        InternalServerError("Twitter credentials missing")
//      }
//    }
//
//  }

}
